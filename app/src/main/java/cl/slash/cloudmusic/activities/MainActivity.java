package cl.slash.cloudmusic.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import cl.slash.cloudmusic.R;
import cl.slash.cloudmusic.utils.DataProvider;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!DataProvider.isLogged()) {
            logout();
        }
        setContentView(R.layout.main_activity);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!DataProvider.isLogged()) {
            logout();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!DataProvider.isLogged()) {
            logout();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void logout() {
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        finish();
    }
}
