package cl.slash.cloudmusic.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import cl.slash.cloudmusic.R;

public class LoginFragment extends Fragment {

    public static final String URL = "url";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        WebView v = (WebView) inflater.inflate(R.layout.basic_webview, container);
         v.loadUrl("www.google.com");
        return v;
    }
}
